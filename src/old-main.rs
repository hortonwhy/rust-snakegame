use yew::prelude::*;

const BOARD_SIZE: i32 = 10;

#[derive(Clone, PartialEq)]
struct Board {
    rows: Vec<Vec<i32>>,
}

#[function_component(App)]
fn app() -> Html {
    let mut board = Board {
        rows: vec![
            vec![0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            vec![0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            vec![0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            vec![0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            vec![0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            vec![0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            vec![0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            vec![0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            vec![0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            vec![0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        ],
    };

    //{ board.rows.iter_mut().flat_map(|r| r.iter_mut()).collect::<Html>() }
    let mut i = 0;
    let result = board.rows.iter().flat_map(|row| row.iter()).map(|value| {
        i += 1;
        html! {
        if i % BOARD_SIZE == 0 {
            <>
        <span>{format!("{} ", value)}</span>
            <br/>
            </>
        } else {
            <>
        <span>{format!("{} ", value)}</span>
            </>
        }
        }
    });
    return result.collect::<Html>();
}

fn main() {
    yew::start_app::<App>();
}
