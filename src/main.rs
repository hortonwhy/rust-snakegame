use gloo::{
    console::{self, Timer},
    timers::callback::{Interval, Timeout},
};
use rand::Rng;
use yew::prelude::*;

const BOARD_SIZE: usize = 12;

pub enum Msg {
    UpdateBoard,
    UpdateDirection(String),
    Update,
    AddSegment,
    AddFood,
}

pub struct Game {
    time: String,
    rows: Vec<Vec<String>>,
    start: Vec<usize>,
    body: Vec<(usize, usize)>,
    direction: Vec<i8>,
    size: usize,
    current_food: i8,
    max_food: i8,
}

impl Game {
    fn get_current_time() -> String {
        let date = js_sys::Date::new_0();
        String::from(date.to_locale_time_string("en-US"))
    }
}

impl Component for Game {
    type Message = Msg;
    type Properties = ();

    fn create(ctx: &Context<Self>) -> Self {
        let mut rows_v = vec![];
        for i in 0..BOARD_SIZE {
            let mut row = vec![];
            for j in 0..BOARD_SIZE {
                let Strng: String = "[ ]".to_string();
                row.push(Strng);
            }
            rows_v.push(row);
        }
        Self {
            time: Game::get_current_time(),
            rows: rows_v,
            start: vec![5, 5],
            direction: vec![-1, 0],
            body: vec![(5, 5)],
            size: 0,
            current_food: 0,
            max_food: 3,
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::UpdateBoard => {
                let link = ctx.link().clone();
                let timeout = Timeout::new(300, move || link.send_message(Msg::Update));
                timeout.forget();
                Some(Timer::new("Timer"));
                true
            }
            Msg::AddSegment => {
                //self.size += 1;
                let mut segment = vec![(self.start[0], self.start[1])];
                self.body.append(&mut segment);
                false
            }

            Msg::AddFood => {
                let mut rng = rand::thread_rng();
                let x: usize = rng.gen_range(0..self.rows[0].len());
                let y: usize = rng.gen_range(0..self.rows[1].len());
                self.rows[x][y] = "[F]".to_string();
                self.current_food += 1;
                false
            }

            Msg::Update => {
                if self.start[0] > BOARD_SIZE {
                    self.start[0] = 0;
                }
                let link = ctx.link().clone();

                if self.current_food < self.max_food {
                    link.send_message(Msg::AddFood);
                }

                if self.rows[self.start[0]][self.start[1]] == "[F]" {
                    self.size += 1;
                    self.current_food -= 1;
                    self.rows[self.start[0]][self.start[1]] = "[ ]".to_string();
                }
                self.rows[self.start[0]][self.start[1]] = "[!]".to_string(); // Head
                                                                             //let tuple = self.body[self.body.len() - 1];
                if self.body.len() > 0 {
                    let tuple = self.body[0];
                    if self.body.len() - 1 > self.size {
                        self.rows[tuple.0][tuple.1] = "[ ]".to_string(); // Tail
                        self.body.remove(0);
                    }
                }
                link.send_message(Msg::AddSegment);

                if self.direction[0] == 1 {
                    if self.start[0] == self.rows[0].len() - 1 {
                        self.start[0] = 0
                    } else {
                        self.start[0] += 1
                    }
                } else if self.direction[0] == -1 {
                    if self.start[0] != 0 {
                        self.start[0] -= 1;
                    } else {
                        self.start[0] = self.rows.len() - 1;
                    }
                }
                if self.direction[1] == 1 {
                    if self.start[1] == self.rows[1].len() - 1 {
                        self.start[1] = 0;
                    } else {
                        self.start[1] += 1
                    }
                } else if self.direction[1] == -1 {
                    if self.start[1] == 0 {
                        self.start[1] = self.rows.len() - 1;
                    } else {
                        self.start[1] -= 1
                    }
                }

                link.send_message(Msg::UpdateBoard);
                true
            }
            Msg::UpdateDirection(s) => {
                if s == "l" {
                    self.direction = vec![0, -1]
                } else if s == "r" {
                    self.direction = vec![0, 1]
                } else if s == "u" {
                    self.direction = vec![-1, 0]
                } else if s == "d" {
                    self.direction = vec![1, 0]
                }
                //self.rows[self.start[0]][self.start[1]] += 1;
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let mut i = 0;
        let onkeydown = ctx.link().batch_callback(|e: KeyboardEvent| {
            e.stop_propagation();
            if e.key() == "ArrowLeft" {
                Some(Msg::UpdateDirection("l".to_string()))
            } else if e.key() == "ArrowRight" {
                Some(Msg::UpdateDirection("r".to_string()))
            } else if e.key() == "ArrowDown" {
                Some(Msg::UpdateDirection("d".to_string()))
            } else if e.key() == "ArrowUp" {
                Some(Msg::UpdateDirection("u".to_string()))
            } else {
                None
            }
        });
        html! {
        <>
            {self.rows
                .iter()
                .flat_map(|row| row.iter())
                .map(|value| {
                    i += 1;
            let mut style ="";
            if value == "[!]" {
            style = "color:blue";
            }

            if value == "[F]" {
            style = "color:orange";
            }
                    html! {
                                <>
                                            if i % BOARD_SIZE == 0 {
                                                <>
                            <span style={style}>{""}{format!("{}", value)}</span>
                                                <br/>
                                                </>
                                            } else {
                        <span style={style}>{""}{format!("{}",value)}</span>
                                }
                            </>

                                            }
                    //});
                }).collect::<Html>()}
                        <button id="btn" onclick={ctx.link().callback(|_| Msg::UpdateBoard)}>
                        {"StartUpdate"}
                        </button>
        <input type="text" {onkeydown} />
        <br/>
        <h1>{"Size "}{self.size}</h1>
        </>
        }
    }
}

fn main() {
    yew::start_app::<Game>();
}
